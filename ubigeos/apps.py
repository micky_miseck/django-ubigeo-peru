from django.apps import AppConfig


class UbigeosConfig(AppConfig):
    name = 'ubigeos'
